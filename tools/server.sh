#!/bin/sh

set -e

# Note the line below: this script is for development only. DO NOT EVER use
# this script for a production deployment.
export FAKE_CERTIFICATE_USER=$USER

case $# in
  0)
    port=8080
    ;;
  1)
    port="$1"
    ;;
  *)
    echo "usage: $0 [PORT]"
    ;;
esac

echo "I: Web UI at http://localhost:$port/"
echo "I: Web UI at https://localhost:$((port + 1))/"
echo "I: Hit Control+C to stop"
echo ""
exec rerun \
  --name $(basename $0 .sh) \
  --no-notify \
  --background \
  --dir lib \
  -p '**/*.rb' -- \
  puma --silent --quiet --include lib --bind=tcp://0.0.0.0:${port} --bind=ssl://0.0.0.0:$((port + 1)) config.ru
