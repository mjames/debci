#!/bin/sh

parallel=parallel
if command -v parallel.moreutils >/dev/null; then
  parallel=parallel.moreutils
fi

children=
for arch in $(./bin/debci config --values-only arch_list); do
  for backend in $(./bin/debci config --values-only backend_list); do
    ./bin/debci worker --arch="${arch}" --backend="${backend}" &
    children="${children} $!"
  done
done
trap "kill ${children}" INT TERM EXIT
sleep infinity
