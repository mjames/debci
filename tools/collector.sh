#!/bin/sh

set -eu

exec rerun \
  --name $(basename $0 .sh) \
  --no-notify \
  --background \
  --dir lib \
  -p '**/*.{rb,erb}' -- \
  ./bin/debci collector
