#!/bin/sh

set -eu

usage() {
  cat <<EOF
usage: debci-setup [OPTIONS]

Options:
  -f, --force               Force update, even if the testbed has already been
                            updated recently.
$@
EOF
}

short_options='f'
long_options='force'

export debci_base_dir=$(readlink -f $(dirname $(readlink -f $0))/..)
. $debci_base_dir/lib/environment.sh
. $debci_base_dir/lib/functions.sh

force=
while true; do
  arg="$1"
  shift
  case "$arg" in
    -f|--force)
      force="$arg"
      ;;
    --)
      break
      ;;
  esac
done


if [ -n "$force" ]; then
  rm -f "$debci_testbed_timestamp"
fi

if [ -e "$debci_testbed_timestamp" ]; then
  last_update=$(stat --format=%Y "$debci_testbed_timestamp")
  now=$(date +%s)
  if [ $(($now - $last_update)) -le 43200 ]; then # 12h
    log "I: testbed setup [${debci_suite}/${debci_arch}/${debci_backend}]: skip (already updated in the last 12h)"
    exit
  fi
fi

log "I: testbed setup [${debci_suite}/${debci_arch}/${debci_backend}]: starting at $(date)"

mkdir -p "$debci_log_dir"
log="$debci_log_dir/debci-setup.$(date +%d).log"
touch "$log"
user_group=$(stat -c %U:%G "${debci_log_dir}")
chown $user_group "$log"
ln -sf "$(basename "$log")" "$debci_log_dir/debci-setup.log"
chown $user_group "$debci_log_dir/debci-setup.log"
find "$debci_log_dir" -mtime +30 -and -name '*.log' -delete

(
  msg="debci-setup started on $(LANG=C date)"
  echo "$msg"
  echo "$msg" | sed -s 's/./-/g'
) >> $log 2>&1

if [ "$debci_quiet" = false ]; then
  maybe_with_proxy mispipe "create-testbed --suite '$debci_suite' 2>&1" "tee --append $log"
else
  maybe_with_proxy create-testbed --suite "$debci_suite" >>$log 2>&1
fi

# record timestamp
touch "$debci_testbed_timestamp"

log "I: testbed setup [${debci_suite}/${debci_arch}/${debci_backend}]: finished at $(date)"
